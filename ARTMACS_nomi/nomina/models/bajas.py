#Django
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.utils import timezone
#Local
from .employee import Employee

class MotivoBaja(models.Model):
    motivo = models.CharField(max_length=35)
    despido = models.BooleanField(default=False)
    is_Active = models.BooleanField(default=True)

    def __str__(self):
        return str(self.motivo)

class Bajas(models.Model):
    empleado = models.ForeignKey(Employee, on_delete=models.CASCADE)
    motivo_baja = models.ForeignKey(MotivoBaja, on_delete=models.CASCADE)
    fecha = models.DateTimeField(auto_now_add=True)
    finiquito_calculado = models.DecimalField(max_digits=13, decimal_places=2, null=True, blank=True)
    liquidacion_calculada = models.DecimalField(max_digits=13, decimal_places=2, null=True, blank=True)
    is_recontratable = models.BooleanField(default=True)
    comenatios_baja = models.CharField(max_length=150, blank=True,null=True)

    class Meta:
        verbose_name = "Bajas"
        verbose_name_plural = "Bajas"
    
    def save(self,*args, **kwargs):
        now = timezone.now()
        empl_baja = Employee.objects.get(id = self.empleado.id)
        empl_baja.is_active = False
        empl_baja.in_active_date = now
        empl_baja.save()
        super().save(*args, **kwargs)

    def __str__(self):
        return str(self.empleado)