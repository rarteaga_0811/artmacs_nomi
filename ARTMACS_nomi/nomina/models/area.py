#Django
from django.db import models

class Area(models.Model):
    Description = models.CharField(max_length=150)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.Description