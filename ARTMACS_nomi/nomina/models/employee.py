from django.db import models
from django.utils.translation import gettext_lazy as _

#local
from .area import Area



class Employee(models.Model):
    class GeneroEmployee(models.TextChoices):
        HOMBRE = 'M', _('Masculino')
        MUJER = 'F', _('Femenino')

    nombre = models.CharField(max_length=80)
    apellido_paterno = models.CharField(max_length=80)
    apellido_materno = models.CharField(max_length=80,blank=True, null=True)
    fecha_nacimienti = models.DateField(null=True, blank=True)
    curp = models.CharField(max_length=25)
    rfc = models.CharField(max_length=20, null=True, blank=True)
    area = models.ForeignKey(Area, on_delete=models.CASCADE, null=True, blank=True)
    genero = models.CharField(
        max_length=2,
        choices=GeneroEmployee.choices)
    is_active = models.BooleanField(default=True)
    in_active_date = models.DateTimeField(blank=True,null=True)
    date_created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.nombre + ' ' +self.apellido_paterno +'-'+self.rfc)


class TelefonoEmployee(models.Model):
    class TipoTelefono(models.TextChoices):
        CELULAR = 'Cel', _('Celular')
        FIJO = 'Fijo', _('Fijo')
    employee = models.ForeignKey(Employee, on_delete=models.CASCADE)
    telefono = models.CharField(max_length=50)
    tipo = models.CharField(
        max_length=8,
        choices=TipoTelefono.choices)
    is_principal = models.BooleanField(default=False)
    is_Active = models.BooleanField(default=True)

