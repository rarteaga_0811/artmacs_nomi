#Django
from django.contrib import admin
from django.contrib.auth.models import User
from django.contrib.auth.models import Group

#local
from .models import Employee, TelefonoEmployee
from .models import Bajas, MotivoBaja, Area


class TelefonoEmployeeInLine(admin.TabularInline):
    model = TelefonoEmployee
    extra = 1

class EmployeeAdmin(admin.ModelAdmin):
    list_display = ['nombre', 'apellido_paterno', 'rfc','is_active']
    inlines = [TelefonoEmployeeInLine,]

class BajasAdmin(admin.ModelAdmin):
    list_display = ['empleado','motivo_baja','fecha']

class MotivoBajasAdmin(admin.ModelAdmin):
    list_display = ['motivo']

class AreaAdmin(admin.ModelAdmin):
    list_display =['Description']

admin.site.register(Employee, EmployeeAdmin)
admin.site.register(Bajas, BajasAdmin)
admin.site.register(MotivoBaja, MotivoBajasAdmin)
admin.site.register(Area, AreaAdmin)

#Unregister
admin.site.unregister(User)
admin.site.unregister(Group)