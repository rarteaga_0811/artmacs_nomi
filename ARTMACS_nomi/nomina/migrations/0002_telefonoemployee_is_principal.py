# Generated by Django 4.1.4 on 2022-12-29 01:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('nomina', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='telefonoemployee',
            name='is_principal',
            field=models.BooleanField(default=False),
        ),
    ]
