# Generated by Django 4.1.4 on 2022-12-29 02:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('nomina', '0004_alter_bajas_options_bajas_comenatios_baja_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='employee',
            name='in_active_date',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
